import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';

export default class Cliente extends Component {
  render() {
    return (
      <View>
        <Text>Current Scene: { this.props.title }</Text>
        <Text>Dados do cliente: (...)</Text>
        <TouchableHighlight onPress={this.props.onBack}>
          <Text>Voltar</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

Cliente.propTypes = {
  title: PropTypes.string.isRequired,
  onBack: PropTypes.func.isRequired,
};
